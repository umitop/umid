// Copyright (c) 2021-2024 UMI
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package syncer

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/umitop/umid/pkg/config"
	"gitlab.com/umitop/umid/pkg/ledger"
	"gitlab.com/umitop/umid/pkg/umi"
)

type Fetcher struct {
	config    *config.Config
	client    *http.Client
	confirmer *ledger.ConfirmerLegacy
}

func NewFetcher(conf *config.Config) *Fetcher {
	return &Fetcher{
		config: conf,
		client: newClient(),
	}
}

func (fetcher *Fetcher) SetConfirmer(confirmer *ledger.ConfirmerLegacy) {
	fetcher.confirmer = confirmer
}

func (fetcher *Fetcher) Worker(ctx context.Context) {
	ticker := time.NewTicker(time.Second * 3)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			for {
				if fetcher.fetchBlocks(ctx) < 1 {
					break
				}
			}

		case <-ctx.Done():
			return
		}
	}
}

func (fetcher *Fetcher) fetchBlocks(ctx context.Context) int {
	ctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	offset := fetcher.confirmer.BlockHeight
	limit := 10_000
	url := fmt.Sprintf("%s/api/blocks?raw=true&limit=%d&offset=%d", fetcher.config.Peer, limit, offset)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		log.Printf("fetch error: %v", err)

		return -1
	}

	response, err := fetcher.client.Do(request)
	if err != nil {
		log.Printf("fetch error: %v", err)

		return -1
	}

	defer response.Body.Close()

	responseBody := struct {
		Data struct {
			TotalCount int      `json:"totalCount"`
			Items      [][]byte `json:"items"`
		} `json:"data"`
	}{}

	if err := json.NewDecoder(response.Body).Decode(&responseBody); err != nil {
		log.Printf("fetch error: %v", err)

		return -1
	}

	if len(responseBody.Data.Items) == 0 {
		return 0
	}

	log.Printf("скачано %d блоков, начиная с %d", len(responseBody.Data.Items), offset)

	fetcher.parseBlocks(responseBody.Data.Items)

	return len(responseBody.Data.Items)
}

func (fetcher *Fetcher) parseBlocks(blocks [][]byte) {
	for _, block := range blocks {
		blk := (umi.BlockLegacy)(block)

		if err := blk.Verify(); err != nil {
			return
		}

		for i, j := 0, blk.TransactionCount(); i < j; i++ {
			if err := blk.Transaction(i).Verify(); err != nil {
				return
			}
		}

		err := fetcher.confirmer.AppendBlockLegacy(block)
		if err != nil {
			log.Printf("error: %v", err)

			return
		}
	}
}
