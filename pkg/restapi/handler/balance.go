package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/umitop/umid/pkg/umi"
)

func Balances(ledger1 iLedger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		if !strings.HasPrefix(r.Header.Get("Content-Type"), "application/json") {
			http.Error(w, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)

			return
		}

		type Request struct {
			Data []string `json:"data"`
		}

		request := Request{}

		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&request); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		type Balance struct {
			Address string `json:"address"`
			Balance uint64 `json:"balance"`
		}

		type Response struct {
			Data []Balance `json:"data"`
		}

		balances := make([]Balance, 0, len(request.Data))
		timestamp := uint32(time.Now().Unix())

		for _, address := range request.Data {
			addr, err := umi.ParseAddress(address)
			if err != nil {
				continue
			}

			balance := uint64(0)

			if acc, ok := ledger1.Account(addr); ok {
				balance = acc.BalanceAt(timestamp)
			}

			balances = append(balances, Balance{
				Address: address,
				Balance: balance,
			})
		}

		response := Response{Data: balances}
		encoder := json.NewEncoder(w)

		err := encoder.Encode(response)
		if err != nil {
			log.Println(err)
		}
	}
}
