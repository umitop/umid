package handler

import (
	"encoding/json"
	"math"
	"net/http"
	"os"
	"time"

	"gitlab.com/umitop/umid/pkg/ledger"
)

func Burner() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		setHeaders(w, r)

		type Data struct {
			NextBurnAt       time.Time `json:"nextBurnAt"`
			UntilNextBurnSec float64   `json:"UntilNextBurnSec"`
		}

		type Resp struct {
			Data Data `json:"data"`
		}

		resp := Resp{
			Data: Data{
				NextBurnAt:       ledger.NextBurn,
				UntilNextBurnSec: math.Round(time.Until(ledger.NextBurn).Seconds()),
			},
		}

		_ = json.NewEncoder(w).Encode(resp)
	}
}

func Burn() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		setHeaders(w, r)

		type Data struct {
			OK bool `json:"ok"`
		}

		type Resp struct {
			Data Data `json:"data"`
		}

		resp := Resp{Data: Data{OK: false}}

		if key, ok := os.LookupEnv("UMI_BURN_KEY"); ok {
			if key == r.Header.Get("X-Legend-Burn-Key") {
				ledger.NextBurn = time.Now().Add(time.Second * 10)
				resp.Data.OK = true
			}
		}

		_ = json.NewEncoder(w).Encode(resp)
	}
}
