// Copyright (c) 2021-2024 UMI
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ledger

import (
	"crypto/ed25519"
	"encoding/base64"
	"log"
	"os"
	"time"

	"gitlab.com/umitop/umid/pkg/umi"
)

type Mempool interface {
	Push(umi.Transaction) error
}

type Burner struct {
	confirmer *ConfirmerLegacy
	mempool   Mempool
}

func NewBurner(confirmer *ConfirmerLegacy, mempool Mempool) *Burner {
	return &Burner{
		confirmer: confirmer,
		mempool:   mempool,
	}
}

const (
	burnDay  = 1
	burnHour = 12
)

var NextBurn = time.Date(time.Now().Year(), time.Now().Month(), burnDay, burnHour, 0, 0, 0, time.UTC)

func (burner *Burner) Run() {
	// На случай, если сегодня день сжигания, и мастер-ноду перезапустили
	if time.Now().After(NextBurn) {
		NextBurn = nextBurn()
	}

	log.Println("Burner started, next tick:", NextBurn.Format("2006-01-02 15:04:05"))

	for {
		if time.Now().UTC().After(NextBurn) {
			NextBurn = nextBurn()
			burn(burner)
		}

		time.Sleep(time.Second)
	}
}

func burn(burner *Burner) {
	secKey, _ := base64.StdEncoding.DecodeString(os.Getenv("UMI_MASTER_KEY"))
	pubKey := secKey[ed25519.PublicKeySize:ed25519.PrivateKeySize]

	log.Println("Burn ROD!")

	for adr, acc := range burner.confirmer.ledger.accounts[umi.PfxVerRod] {
		if acc.Type != umi.Umi {
			continue
		}

		balance := acc.BalanceAt(burner.confirmer.BlockTimestamp)

		if balance == 0 {
			continue
		}

		log.Println("Burn", adr.String(), acc.BalanceAt(burner.confirmer.BlockTimestamp))

		snd := umi.Address{}
		snd.SetPrefix(umi.PfxVerRod)
		snd.SetPublicKey(pubKey)

		tx := umi.NewTransaction()
		tx.SetVersion(umi.TxV18BurnRod)
		tx.SetSender(snd)
		tx.SetRecipient(adr)
		tx.SetAmount(balance)
		tx.SetTimestamp(uint32(time.Now().Unix()))
		tx.SetNonce(uint32(time.Now().Nanosecond()))

		copy(tx[86:150], ed25519.Sign(secKey, tx[0:86]))

		err := burner.mempool.Push(tx)
		if err != nil {
			log.Println(err)
		}
	}
}

func nextBurn() time.Time {
	return time.Date(time.Now().Year(), time.Now().Month()+1, burnDay, burnHour, 0, 0, 0, time.UTC)
}
