package umi

type TxIndex struct {
	BlockHeight uint32
	TxIdx       uint16
}
