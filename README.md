# umid

[![goreportcard](https://goreportcard.com/badge/gitlab.com/umitop/umid)](https://goreportcard.com/report/gitlab.com/umitop/umid)
[![Codeac](https://static.codeac.io/badges/3-20277317.svg "Codeac")](https://app.codeac.io/gitlab/umitop/umid)

## Build:

```go build -o umid ./cmd/umid```
